package com.cat.marveltask.data.cloud

import com.cat.marveltask.data.entity.CharacterEntity
import com.cat.marveltask.data.entity.DataWrapperEntity
import com.cat.marveltask.data.entity.MediaEntity
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Ahmed on 7/26/2019.
 */
interface MarvelAPI {
    @GET("v1/public/characters")
    suspend fun getCharacters(
        @Query("apikey") apiKey: String,
        @Query("hash") hash: String,
        @Query("ts") timestamp: String,
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    )
            : Response<DataWrapperEntity<CharacterEntity>>

    @GET("v1/public/characters")
    suspend fun getCharacters(
        @Query("apikey") apiKey: String,
        @Query("hash") hash: String,
        @Query("ts") timestamp: String,
        @Query("name") name: String
    )
            : Response<DataWrapperEntity<CharacterEntity>>

    @GET("v1/public/characters/{characterId}/comics")
    suspend fun getComics(
        @Path("characterId") id: Int,
        @Query("apikey") apiKey: String,
        @Query("hash") hash: String,
        @Query("ts") timestamp: String
    )
            : Response<DataWrapperEntity<MediaEntity>>

    @GET("v1/public/characters/{characterId}/series")
    suspend fun getSeries(
        @Path("characterId") id: Int,
        @Query("apikey") apiKey: String,
        @Query("hash") hash: String,
        @Query("ts") timestamp: String
    )
            : Response<DataWrapperEntity<MediaEntity>>

    @GET("v1/public/characters/{characterId}/stories")
    suspend fun getStories(
        @Path("characterId") id: Int,
        @Query("apikey") apiKey: String,
        @Query("hash") hash: String,
        @Query("ts") timestamp: String
    )
            : Response<DataWrapperEntity<MediaEntity>>

    @GET("v1/public/characters/{characterId}/events")
    suspend fun getEvents(
        @Path("characterId") id: Int,
        @Query("apikey") apiKey: String,
        @Query("hash") hash: String,
        @Query("ts") timestamp: String
    )
            : Response<DataWrapperEntity<MediaEntity>>
}