package com.cat.marveltask.data.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by Ahmed on 7/28/2019.
 */
class URLEntity {
    @SerializedName("type")
    var type: String = ""
    @SerializedName("url")
    var url: String = ""
}