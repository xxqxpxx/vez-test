package com.cat.marveltask.domain.repository

import com.cat.marveltask.domain.model.CharacterMediaModel
import com.cat.marveltask.domain.model.CharacterModel
import com.cat.marveltask.domain.model.DataWrapperModel

/**
 * Created by Ahmed on 7/26/2019.
 */
interface ICharacterRepository {
    suspend fun getCharacters(page: Int): DataWrapperModel<List<CharacterModel>>

    suspend fun getCharacters(name: String): DataWrapperModel<List<CharacterModel>>

    suspend fun getCharacterMedia(id: Int): DataWrapperModel<CharacterMediaModel>

}