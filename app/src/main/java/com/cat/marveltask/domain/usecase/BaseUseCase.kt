package com.cat.marveltask.domain.usecase

import com.cat.marveltask.domain.model.DataWrapperModel

/**
 * Created by Ahmed on 7/26/2019.
 */
abstract class BaseUseCase<Param, Data> {
    abstract suspend fun execute(param: Param): DataWrapperModel<Data>
}