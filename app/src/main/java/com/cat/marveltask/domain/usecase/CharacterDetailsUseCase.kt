package com.cat.marveltask.domain.usecase

import com.cat.marveltask.domain.model.CharacterMediaModel
import com.cat.marveltask.domain.model.DataWrapperModel
import com.cat.marveltask.domain.repository.ICharacterRepository
import javax.inject.Inject

/**
 * Created by Ahmed on 7/28/2019.
 */
class CharacterDetailsUseCase @Inject constructor(val iCharacterRepository: ICharacterRepository) :
    BaseUseCase<Int, CharacterMediaModel>() {

    //Gets all media including comics, series, stories and events
    override suspend fun execute(param: Int): DataWrapperModel<CharacterMediaModel> {
        return iCharacterRepository.getCharacterMedia(param)
    }

}