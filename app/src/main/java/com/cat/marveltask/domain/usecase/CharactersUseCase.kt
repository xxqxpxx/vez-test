package com.cat.marveltask.domain.usecase

import com.cat.marveltask.domain.model.CharacterModel
import com.cat.marveltask.domain.model.DataWrapperModel
import com.cat.marveltask.domain.repository.ICharacterRepository
import javax.inject.Inject

/**
 * Created by Ahmed on 7/26/2019.
 */
class CharactersUseCase @Inject constructor(val iCharacterRepository: ICharacterRepository) :
    BaseUseCase<Int, List<CharacterModel>>() {


    override suspend fun execute(param: Int): DataWrapperModel<List<CharacterModel>> {
        return iCharacterRepository.getCharacters(param)
    }

}