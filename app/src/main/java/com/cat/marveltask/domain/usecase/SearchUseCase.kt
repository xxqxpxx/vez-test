package com.cat.marveltask.domain.usecase

import com.cat.marveltask.domain.model.CharacterModel
import com.cat.marveltask.domain.model.DataWrapperModel
import com.cat.marveltask.domain.repository.ICharacterRepository
import javax.inject.Inject

/**
 * Created by Ahmed on 7/27/2019.
 */
class SearchUseCase @Inject constructor(val iCharacterRepository: ICharacterRepository) :
    BaseUseCase<String, List<CharacterModel>>() {

    override suspend fun execute(param: String): DataWrapperModel<List<CharacterModel>> {
        return iCharacterRepository.getCharacters(param)
    }

}