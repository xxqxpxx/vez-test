package com.cat.marveltask.presentation.dagger.component

import com.cat.marveltask.domain.repository.ICharacterRepository
import com.cat.marveltask.presentation.dagger.module.DataModule
import com.cat.marveltask.presentation.view.fragment.CharacterDetailsFragment
import com.cat.marveltask.presentation.view.fragment.CharactersFragment
import com.cat.marveltask.presentation.view.fragment.SearchFragment
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Ahmed on 7/26/2019.
 */
@Singleton
@Component(modules = [DataModule::class])
interface FragmentComponent {
    fun inject(searchFragment: SearchFragment)
    fun inject(charactersFragment: CharactersFragment)
    fun inject(characterDetailsFragment: CharacterDetailsFragment)

    //dependencies
    fun characterRepository(): ICharacterRepository
}