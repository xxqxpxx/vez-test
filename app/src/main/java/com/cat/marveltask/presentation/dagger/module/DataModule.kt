package com.cat.marveltask.presentation.dagger.module

import android.app.Activity
import com.cat.marveltask.data.repository.CharacterRepository
import com.cat.marveltask.domain.repository.ICharacterRepository
import dagger.Module
import dagger.Provides

/**
 * Created by Ahmed on 7/26/2019.
 */
@Module
class DataModule(val activity: Activity) {

    //A repository needs context to create room database
    @Provides
    fun provideRepository(): ICharacterRepository = CharacterRepository(activity)
}