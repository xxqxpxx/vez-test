package com.cat.marveltask.presentation.viewModel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.cat.marveltask.domain.model.CharacterMediaModel
import com.cat.marveltask.domain.model.DataWrapperModel
import com.cat.marveltask.domain.usecase.CharacterDetailsUseCase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Ahmed on 7/28/2019.
 */
class CharacterDetailsViewModel @Inject constructor(private val characterUseCase: CharacterDetailsUseCase) :
    ViewModel() {

    private val mediaContainerLiveData = MutableLiveData<DataWrapperModel<CharacterMediaModel>>()

    private val job = SupervisorJob()

    private val coroutineContext = Dispatchers.IO + job

    fun getMedia(id: Int): LiveData<DataWrapperModel<CharacterMediaModel>> {
        updateSearch(id)
        return mediaContainerLiveData
    }

    fun updateSearch(id: Int) {

        CoroutineScope(coroutineContext).launch {
            mediaContainerLiveData.postValue(characterUseCase.execute(id))
        }
    }
}